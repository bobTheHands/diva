/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict';

// @see https://umbrellajs.com
/* global u */

// @see ./diva-ui.js
/* global Ui */

// @see ./diva-culture.js
/* global UiCulture */

// @see https://developer.mozilla.org/en-US/docs/Web/API/WebSocket
/* global WebSocket */

// @see @see https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
/* global fetch */

if (!u || !Ui || !UiCulture || !WebSocket || !fetch) {
  throw new Error('invalid state');
}

class UiTrade {

  static contract = '';
  static buy = [];
  static sell = [];
  static marketBuy = [];
  static marketSell = [];

  static make (_contract = '') {
    UiTrade.contract = _contract || u('select#contract').first().value;

    UiTrade.TYPE_BUY = 'buy';
    UiTrade.TYPE_SELL = 'sell';

    UiTrade.CHANNEL_NOSTRO = 'nostro';
    UiTrade.CHANNEL_MARKET = 'market';

    UiTrade.COMMAND_SUBSCRIBE = 'subscribe';
    UiTrade.COMMAND_UNSUBSCRIBE = 'unsubscribe';
    UiTrade.COMMAND_ADD = 'add';
    UiTrade.COMMAND_DELETE = 'delete';
    UiTrade.COMMAND_CONFIRM = 'confirm';

    UiTrade.CLASS_PENDING_ADDITION = 'pending-addition';
    UiTrade.CLASS_PENDING_DELETION = 'pending-deletion';

    UiTrade._refresh();

    // Listen for data
    Ui.websocket.addEventListener('message', async (event) => {
      try {
        const obj = JSON.parse(event.data);
        //@FIXME
        await UiTrade._process(obj);
      } catch (error) {}
    });

    UiTrade._attachEvents();
  }

  static _process(obj) {
    if (obj.contract !== UiTrade.contract) {
      return;
    }
    obj.buy.sort((a, b) => a.p.padStart(21, '0') > b.p.padStart(21, '0') ? -1 : 1);
    obj.sell.sort((a, b) => a.p.padStart(21, '0') > b.p.padStart(21, '0')  ? 1 : -1);
    if (obj.channel === UiTrade.CHANNEL_NOSTRO) {
      UiTrade._htmlNostroOrder(Object.values(obj.buy), Object.values(obj.sell));
    }
    if (obj.channel === UiTrade.CHANNEL_MARKET) {
      UiTrade._htmlMarketOrder(Object.values(obj.buy), Object.values(obj.sell));
    }
  }

  /**
   * @private
   */
  static _refresh() {
    if (Ui.websocket.readyState !== WebSocket.OPEN) {
      return setTimeout(() => { UiTrade._refresh(); }, 100);
    }

    UiTrade.buy = [];
    UiTrade.sell = [];
    UiTrade.marketBuy = [];
    UiTrade.marketSell = [];

    if (UiTrade.contract && UiTrade.contract !== u('select#contract').first().value) {
      u('span.logo-contract').removeClass(UiTrade.contract);
      // unsubscribe from book updates
      Ui.websocket.send(JSON.stringify({
        channel: UiTrade.CHANNEL_NOSTRO,
        command: UiTrade.COMMAND_UNSUBSCRIBE,
        contract: UiTrade.contract
      }));

      Ui.websocket.send(JSON.stringify({
        channel: UiTrade.CHANNEL_MARKET,
        command: UiTrade.COMMAND_UNSUBSCRIBE,
        contract: UiTrade.contract
      }));
    }

    UiTrade.contract = u('select#contract').first().value;
    u('span.logo-contract').addClass(UiTrade.contract);

    Ui.websocket.send(JSON.stringify({
      channel: UiTrade.CHANNEL_NOSTRO,
      command: UiTrade.COMMAND_SUBSCRIBE,
      contract: UiTrade.contract
    }));

    Ui.websocket.send(JSON.stringify({
      channel: UiTrade.CHANNEL_MARKET,
      command: UiTrade.COMMAND_SUBSCRIBE,
      contract: UiTrade.contract
    }));

    // nostro
    u('article.orderbook div.panel-block').addClass('is-hidden');
    u('#' + u('article.orderbook a.is-active').data('tab')).removeClass('is-hidden');
  }

  /**
   * @private
   */
  static _attachEvents () {
    // form order
    u('form#order').off('submit').handle('submit', () => {
      UiTrade._uxAmountPriceIsValid() && UiTrade._order();
    })

    // amount,price
    u('input#amount,input#price').off('keyup').handle('keyup', () => {
      UiTrade._uxEnableDisableOrderButton();
    });

    // contract
    u('select#contract').off('change').handle('change', async (e) => {
      UiTrade._uxClearAmountPrice();
      await UiTrade._changeContract(e.target.value);
    });

    // type
    u('#type').off('change').handle('change', async (e) => {
      UiTrade._uxClearAmountPrice();
      await UiTrade._changeType(e.target.value);
    });

    // nostro tabs
    u('article.orderbook .panel-tabs a').off('click').handle('click', (e) => {
      u('article.orderbook .panel-tabs a').removeClass('is-active');
      u(e.currentTarget).addClass('is-active');
      u('article.orderbook div.panel-block').addClass('is-hidden');
      u('#' + u('article.orderbook a.is-active').data('tab')).removeClass('is-hidden');
    });

    // delete nostro order
    u('div#nostro-order td.action button[data-did]').off('click').handle('click', (e) => {
      const el = u(e.currentTarget);
      UiTrade._delete(el.data('did'), el.data('t'), el.data('p'), el.data('a'));
    });
  }

  static _uxClearAmountPrice() {
    u('input#amount,input#price').each(e => e.value = '');
    UiTrade._uxEnableDisableOrderButton();
  }

  static _uxAmountPriceIsValid() {
    return !u('input#amount').is(':invalid') && !u('input#price').is(':invalid');
  }

  static _uxEnableDisableOrderButton() {
    UiTrade._uxAmountPriceIsValid()
      ? u('button#place-order').removeClass('is-hidden') && u('button#place-order-disabled').addClass('is-hidden')
      : u('button#place-order').addClass('is-hidden') && u('button#place-order-disabled').removeClass('is-hidden');
  }

  /**
   * @param {string} contract
   * @private
   */
  static async _changeContract (contract) {
    UiTrade._refresh();

    const response = await fetch('/trade/contract', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({contract: contract})
    });

    return UiTrade._handleResponse(response);
  }

  /**
   * @param {string} type
   * @private
   */
  static async _changeType (type) {
    const response = await fetch('/trade/type', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ type: type })
    });

    return UiTrade._handleResponse(response);
  }

  /**
   * @private
   */
  static _order () {
    const type = u('#type').first().value;
    const amount = u('#amount').first().value.trim() || 0;
    const price = u('#price').first().value.trim() || 0;
    if (Number(price) < 0 || Number(amount) <= 0) {
      return;
    }

    u('button#place-order').addClass('is-loading is-disabled');
    setTimeout(() => u('button#place-order').removeClass('is-loading is-disabled'), 1000);
    const json = {
      seq: 1,
      command: UiTrade.COMMAND_ADD,
      contract: UiTrade.contract,
      type: type,
      price: price.toString(),
      amount: amount.toString(),
      id: Date.now()
    };

    Ui.websocket.send(JSON.stringify(json));
  }

  /**
   * @private
   */
  static _delete (id, type, price, amount) {
    const json = {
      seq: 1,
      contract: UiTrade.contract,
      command: UiTrade.COMMAND_DELETE,
      id: Number(id),
      type: type,
      price: price,
      amount: amount
    };

    Ui.websocket.send(JSON.stringify(json));
  }

  /**
   * @private
   */
  static async _handleResponse (res) {
    if (!res.ok) {
      res.text().then((html) => Ui.message('ERROR', html));
    }
  }

  static _htmlNostroOrder(arrayBuy, arraySell) {
    let s = '';
    while (arrayBuy.length || arraySell.length) {
      const buy = arrayBuy.length ? arrayBuy.shift() : '';
      const sell = arraySell.length ? arraySell.shift() : '';
      s = s + `<tr><td class="action">${buy.p ? UiTrade._htmlButtonTrash(buy.id, 'buy', buy.p, buy.a): ''}</td>` +
        `<td class="buy">${UiTrade._htmlPriceAmount(buy.p, buy.a)}</td>` +
        `<td class="sell">${UiTrade._htmlPriceAmount(sell.p, sell.a)}</td>` +
        `<td class="action">${sell.p ? UiTrade._htmlButtonTrash(sell.id, 'sell', sell.p, sell.a): ''}</td></tr>`;
    }
    u('#nostro-order table tbody').html(s);
    UiTrade._attachEvents();
  }

  static _htmlPriceAmount(price, amount) {
    return price ? `<div class="price">${price}</div><div class="amount">${amount}</div>` : '';
  }

  static _htmlButtonTrash(id, type, price, amount) {
    return `<button data-did="${id}" data-t="${type}" data-p="${price}" data-a="${amount}" class="button is-inverted">
        <span class="icon"><i class="fas fa-trash"></i></span>
      </button>`;
  }

  static _htmlMarketOrder(arrayBuy, arraySell) {
    let s = '';
    while (arrayBuy.length || arraySell.length) {
      const buy = arrayBuy.length ? arrayBuy.shift() : '';
      const sell = arraySell.length ? arraySell.shift() : '';
      s = s + `<tr><td class="action"></td><td class="buy">${UiTrade._htmlPriceAmount(buy.p, buy.a)}</td>` +
        `<td class="sell">${UiTrade._htmlPriceAmount(sell.p, sell.a)}</td><td class="action"></td></tr>`;
    }
    u('#market-order table tbody').html(s);
    UiTrade._attachEvents();
  }
}

