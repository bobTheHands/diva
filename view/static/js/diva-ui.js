/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict';

// @see https://umbrellajs.com
/* global u */

// @see ./diva-culture.js
/* global UiCulture */

// @see https://developer.mozilla.org/en-US/docs/Web/API/WebSocket
/* global WebSocket */

if (!u || !WebSocket || !UiCulture) {
  throw new Error('invalid state');
}

class Ui {
  /**@type WebSocket */
  static websocket = null;

  /**
   * @public
   */
  static make () {
    u('#modal-notification').removeClass('is-active');
    u('#modal-notification .message-header p').text('');
    u('#modal-notification .message-body p').text('');

    // mobile menu
    u('.navbar-burger').off('click').handle('click', () => {
      // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
      u('.navbar-burger').toggleClass('is-active')
      u('.navbar-menu').toggleClass('is-active')
    })

    Ui._attachEvents();

    Ui.connectWebSocket();
  }

  static connectWebSocket() {
    // connect to local protocol websocket
    //@FIXME port
    Ui.websocket = new WebSocket('ws://' + document.location.hostname + ':19720');

    // Connection opened
    Ui.websocket.addEventListener('open', () => {
      u('#status').addClass('has-text-success');
      u('#status').removeClass('has-text-danger');
    });

    // Closing
    Ui.websocket.addEventListener('close', async () => {
      Ui.websocket = null;
      u('#status').removeClass('has-text-success');
      u('#status').addClass('has-text-danger');
      setTimeout(() => { Ui.connectWebSocket(); }, 2000);
    });
  }

  /**
   * @param urlScript {string}
   * @param onLoad {Function|null}
   * @param onError {Function|null}
   * @public
   */
  static load (urlScript, onLoad = null, onError = null) {
    // check if DOM is already available
    if (document.readyState === 'complete' || document.readyState === 'interactive') {
      Ui._load(urlScript, onLoad, onError);
    } else {
      document.addEventListener('DOMContentLoaded', () => Ui._load(urlScript, onLoad, onError));
    }
  }

  /**
   * @param urlScript {string}
   * @param onLoad {Function|null}
   * @param onError {Function|null}
   * @private
   */
  static _load (urlScript, onLoad, onError) {
    const element = document.createElement('script');
    if (typeof onLoad === 'function') {
      element.onload = () => {
        onLoad(urlScript);
      };
    }
    element.onerror = () => {
      if (typeof onError !== 'function') {
        throw new Error(urlScript);
      } else {
        onError(urlScript);
      }
    };
    element.async = true;
    element.src = urlScript;
    u('body').append(element);
  }

  /**
   * @param header {string}
   * @param body {string}
   */
  static message (header, body) {
    u('#modal-notification .message-header p').text(header);
    u('#modal-notification .message-body p').text(body);
    u('#modal-notification').addClass('is-active');
  }

  /**
   * @private
   */
  static _attachEvents () {
    u('#modal-notification .modal-background, #modal-notification button.delete').off('click')
      .handle('click', () => {
        u('#modal-notification').removeClass('is-active');
        u('#modal-notification .message-header p').text('');
        u('#modal-notification .message-body p').text('');
      });
  }
}

// check if DOM is already available
if (document.readyState === 'complete' || document.readyState === 'interactive') {
  // call on next available tick
  setTimeout(Ui.make, 1);
} else {
  document.addEventListener('DOMContentLoaded', Ui.make);
}
