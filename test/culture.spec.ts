/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import { suite, test } from '@testdeck/mocha';
import { expect } from 'chai';
import { Culture, CULTURE_DEFAULT_LANGUAGE_IDENT } from '../src/view/culture';

@suite
class TestCulture {
  @test
  make() {
    expect(Culture.make().uiLanguage).eq(CULTURE_DEFAULT_LANGUAGE_IDENT);
    expect(Culture.make('de').uiLanguage).eq('de');
    // coverage
    Culture.reload();
  }

  @test
  getListIdentLanguages() {
    const a = Culture.getListIdentLanguages();
    expect(a).length.gt(0);
    expect(a.indexOf(CULTURE_DEFAULT_LANGUAGE_IDENT)).gt(-1);
  }

  @test
  notTranslated() {
    expect(Culture.t('---')).eq('---');
    expect(Culture.t(['---', '@@@'])).instanceof(Map);
  }

  @test
  translateString() {
    expect(Culture.translateString('---')).eq('---');
  }

  @test
  translateArray() {
    expect(Culture.translateArray(['abc', '123'])).instanceof(Map);
  }

  @test
  dateTimeTranslation() {
    expect(Culture.t('123', ['de'])).length.gt(0);

    Culture.make(CULTURE_DEFAULT_LANGUAGE_IDENT);
    expect(Culture.formatDateTime(123)).length.gt(0);
    expect(Culture.formatDateTime(123, '', '@')).eq('@');
  }
}
