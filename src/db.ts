/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import fs from 'fs-extra';
import path from 'path';
import Database from 'better-sqlite3';

const NAME_DATABASE_DEFAULT = 'diva';

export class Db {
  private static instance: { [key: string]: Db } = {};
  private readonly nameDatabase: string;
  private readonly db;

  /**
   * Create a new database
   *
   * @param {string} nameDatabase
   * @public
   */
  public static create(nameDatabase: string) {
    new Db(nameDatabase, false).install();
  }

  /**
   * Connect to an existing database
   *
   * @param {string} nameDatabase
   * @return {Db}
   * @public
   */
  public static connect(nameDatabase = ''): Db {
    nameDatabase = nameDatabase || process.env.NAME_DATABASE || NAME_DATABASE_DEFAULT;
    Db.instance[nameDatabase] = Db.instance[nameDatabase] || new Db(nameDatabase);
    return Db.instance[nameDatabase];
  }

  /**
   * @param {string} nameDatabase
   * @param {boolean} mustExist
   * @throws {Error}
   * @private
   */
  private constructor(nameDatabase: string, mustExist: boolean = true) {
    this.nameDatabase = nameDatabase;

    const pathDB = path.join(__dirname, `/../data/${this.nameDatabase}.sqlite`);
    if (!mustExist && fs.existsSync(pathDB)) {
      throw new Error('Database exists');
    }

    this.db = new Database(pathDB, {
      fileMustExist: mustExist,
    });

    // enable foreign keys
    this.db.pragma('foreign_keys = ON');
  }

  /**
   * @return {Db}
   * @throws {Error}
   * @private
   */
  private install() {
    const pathModel = path.join(__dirname, `/../model/install-${this.nameDatabase}.sql`);
    if (!fs.existsSync(pathModel)) {
      throw new Error('Database model not found: ' + pathModel);
    }
    this.db.exec(fs.readFileSync(pathModel, 'utf8'));
    return this;
  }

  /**
   * Get current Date/Time, UTC
   *
   * @param {string} format - Defaults to %Y-%m-%d %H:%M:%f
   * @return {string}
   * @throws {Error}
   * @see https://www.sqlite.org/lang_datefunc.html
   */
  public getNowUtc(format: string = '%Y-%m-%d %H:%M:%f') {
    if (!format.match(/^[a-z\-:% ]+$/i)) {
      throw new Error('invalid format');
    }
    return this.db.prepare("SELECT STRFTIME('" + format + "', 'NOW') as dt").get().dt;
  }

  /**
   * Fetch all records as an array
   *
   * @param {string} sqlSelect
   * @param {Object} params - Named bind params, like @firstname
   * @param {boolean} raw - Set to true to return each row as an array instead of an object, defaults to false
   * @return {Array}
   * @throws {Error}
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#allbindparameters---array-of-rows
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#rawtogglestate---this
   */
  public allAsArray(sqlSelect: string, params = {}, raw = false) {
    return this.db.prepare(sqlSelect).raw(raw).all(params);
  }

  /**
   * Fetch first record as an object
   *
   * @param {string} sqlSelect
   * @param {Object} params - Named bind params, like @firstname
   * @return {Object|undefined} Returns undefined, if no data was found
   * @throws {Error}
   * @public
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#getbindparameters---row
   */
  public firstAsObject(sqlSelect: string, params = {}) {
    return this.db.prepare(sqlSelect).get(params);
  }

  /**
   * Begin Transaction
   */
  public begin() {
    this.prepareRun('BEGIN');
  }

  /**
   * Commit Transaction
   */
  public commit() {
    this.prepareRun('COMMIT');
  }

  /**
   * Rollback Transaction
   */
  public rollback() {
    this.prepareRun('ROLLBACK');
  }

  /**
   * Rollback Transaction
   */
  public vacuum() {
    this.prepareRun('VACUUM');
  }

  /**
   * Insert data, pass a valid INSERT SQL statement
   *
   * @param {string} sqlInsert - Valid INSERT SQL statement
   * @param {Object|Array<Object>} params - Named bind params, like @firstname
   * @return {Object} Info object
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#runbindparameters---object
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#binding-parameters
   */
  public insert(sqlInsert: string, params = {}) {
    if (Array.isArray(params)) {
      let info = {};
      const insert = this.db.prepare(sqlInsert);
      params.forEach((p) => {
        info = insert.run(p);
      });
      return info;
    }
    return this.prepareRun(sqlInsert, params);
  }

  /**
   * Update data, pass a valid UPDATE SQL statement
   *
   * @param {string} sqlUpdate - Valid UPDATE SQL statement
   * @param {Object} params - Named bind params, like @firstname
   * @return {Object} Info object
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#runbindparameters---object
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#binding-parameters
   */
  public update(sqlUpdate: string, params = {}) {
    return this.prepareRun(sqlUpdate, params);
  }

  /**
   * Delete data, pass a valid DELETE SQL statement
   *
   * @param {string} sqlDelete - Valid DELETE SQL statement
   * @param {Object} params - Named bind params, like @firstname
   * @return {Object} Info object
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#runbindparameters---object
   * @see https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md#binding-parameters
   */
  public delete(sqlDelete: string, params = {}) {
    return this.prepareRun(sqlDelete, params);
  }

  /**
   * @param {string} sql - Valid SQL statement
   * @param {Object} params - Named bind params, like @firstname
   * @return {Object} Info object
   */
  private prepareRun(sql: string, params = {}) {
    return this.db.prepare(sql).run(params);
  }
}
