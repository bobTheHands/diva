/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import sodium from 'sodium-native';
import { Db } from '../db';
import { KeyStore } from './key-store';

const REGEX_IDENT_ACCOUNT =
  /^[a-z_0-9]{1,32}@([a-zA-Z]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)*[a-zA-Z]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?$/;
const REGEX_PASSWORD = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[-+_,;£~$!%*#?&])[A-Za-z\d\-+_,;£~$!%*#?&]{10,32}$/;

export class User {
  private identAccount: string;
  private domain: string;
  private readonly db: Db;
  private readonly bufferPassword: Buffer;
  private bufferPublicKey: Buffer;
  private bufferNonce: Buffer;
  private bufferPrivateKeyEncrypted: Buffer;

  /**
   * @param {string} identDomain - Omit or use an asterisk (*) to get all users of all domains
   * @return {Array} Array of records (a record is an object: {account_ident, domain_ident})
   */
  public static allAsArray(identDomain: string = '*') {
    let sql = `SELECT account_ident, domain_ident FROM user
      WHERE @domain_ident = @domain_ident`;
    if (identDomain !== '*') {
      sql += ' AND domain_ident = @domain_ident';
    }
    return Db.connect().allAsArray(sql, { domain_ident: identDomain });
  }

  /**
   * Create a new User
   *
   * @param {string} identAccount
   * @param {string} password
   * @return {Promise<User>}
   * @throws {Error} If creation fails
   * @public
   */
  public static async create(identAccount: string, password: string = 'Password#123') {
    if (!identAccount.match(REGEX_IDENT_ACCOUNT)) {
      throw new Error('Invalid account');
    }

    return new User(password, identAccount).createKeyPairSign().add();
  }

  /**
   * Open an existing user
   *
   * @param {string} identAccount
   * @param {string} password
   * @return {User}
   * @throws {Error} If user is not accessible/available
   * @public
   */
  public static open(identAccount: string, password: string = 'Password#123') {
    if (!identAccount.match(REGEX_IDENT_ACCOUNT)) {
      throw new Error('Invalid account');
    }

    return new User(password, identAccount).fetch();
  }

  /**
   * Delete an existing user
   *
   * @param {string} identAccount
   * @param {string} password
   * @throws {Error} If deletion fails
   * @public
   */
  public static delete(identAccount: string, password: string) {
    if (!identAccount.match(REGEX_IDENT_ACCOUNT)) {
      throw new Error('Invalid account');
    }
    new User(password, identAccount).delete();
  }

  /**
   * @param {string} password
   * @param {string} identAccount
   */
  private constructor(password: string = 'Password#123', identAccount: string) {
    if (!password.match(REGEX_PASSWORD)) {
      throw new Error('Invalid password');
    }
    this.identAccount = '';
    if (identAccount.match(/^[^@]+@[^@]+$/)) {
      this.identAccount = identAccount;
      this.domain = identAccount.split('@')[1];
    } else {
      this.domain = identAccount;
    }

    this.db = Db.connect();

    this.bufferPassword = sodium.sodium_malloc(password.length);
    sodium.sodium_mlock(this.bufferPassword);
    this.bufferPassword.fill(password);

    this.bufferPublicKey = new Buffer('');
    this.bufferNonce = new Buffer('');
    this.bufferPrivateKeyEncrypted = new Buffer('');
  }

  /**
   * @return {string}
   */
  public getAccountIdent(): string {
    return this.identAccount;
  }

  /**
   * @return {string} Public key, hex format
   */
  public getPublicKey(): string {
    return this.bufferPublicKey.toString('hex');
  }

  /**
   * Fetch a user from the database
   *
   * @return {User}
   */
  private fetch(): User {
    const arrayUser = this.db.allAsArray('SELECT * FROM user WHERE account_ident = @account_ident', {
      account_ident: this.identAccount,
    });

    // verify the password
    if (
      arrayUser.length === 1 &&
      sodium.crypto_pwhash_str_verify(Buffer.from(arrayUser[0].passwordhash, 'hex'), this.bufferPassword)
    ) {
      this.bufferNonce = Buffer.from(arrayUser[0].nonce, 'hex');
      this.bufferPublicKey = Buffer.from(arrayUser[0].publickey, 'hex');
      this.bufferPrivateKeyEncrypted = Buffer.from(arrayUser[0].privatekeyenc, 'hex');

      /** @type {Buffer} */
      const bufferPrivateKey = sodium.sodium_malloc(
        this.bufferPrivateKeyEncrypted.byteLength - sodium.crypto_secretbox_MACBYTES
      );
      sodium.sodium_mlock(bufferPrivateKey);

      /** @type {Buffer} */
      const bufferSecret = sodium.sodium_malloc(sodium.crypto_secretbox_KEYBYTES);
      sodium.sodium_mlock(bufferSecret);
      bufferSecret.fill(this.bufferPassword.toString());

      if (
        !sodium.crypto_secretbox_open_easy(
          bufferPrivateKey,
          this.bufferPrivateKeyEncrypted,
          this.bufferNonce,
          bufferSecret
        )
      ) {
        throw new Error('Access denied');
      }
      KeyStore.make().set(this.identAccount + ':keyPrivate', bufferPrivateKey);
      sodium.sodium_munlock(bufferPrivateKey);
      sodium.sodium_munlock(bufferSecret);
    } else {
      throw new Error('User not available');
    }

    return this;
  }

  /**
   * Add a new User
   *
   * @return {User}
   * @throws {Error} On invalid crypto keys
   */
  private add(): User {
    this.db.insert(
      'INSERT INTO user (account_ident, domain_ident, passwordhash, nonce, publickey, privatekeyenc) VALUES \
      (@account_ident, @domain_ident, @passwordhash,  @nonce, @publickey, @privatekeyenc)',
      {
        account_ident: this.identAccount,
        domain_ident: this.domain,
        passwordhash: this.hashPassword(),
        nonce: this.bufferNonce.toString('hex'),
        publickey: this.bufferPublicKey.toString('hex'),
        privatekeyenc: this.bufferPrivateKeyEncrypted.toString('hex'),
      }
    );

    return this;
  }

  /**
   * Delete a User
   *
   * @throws {Error} If deletion fails
   */
  private delete() {
    this.db.delete('DELETE FROM user WHERE account_ident = @account_ident', {
      account_ident: this.identAccount,
    });

    this.identAccount = '';
    this.domain = '';
    sodium.sodium_munlock(this.bufferPassword);

    sodium.sodium_memzero(this.bufferNonce);
    sodium.sodium_memzero(this.bufferPublicKey);
    sodium.sodium_memzero(this.bufferPrivateKeyEncrypted);
  }

  /**
   * Create a password hash from password
   *
   * @return {string} Password hash as a hex string
   */
  private hashPassword(): string {
    /** @type {Buffer} */
    const bufferPasswordHash: Buffer = sodium.sodium_malloc(sodium.crypto_pwhash_STRBYTES);
    sodium.crypto_pwhash_str(
      bufferPasswordHash,
      this.bufferPassword,
      sodium.crypto_pwhash_OPSLIMIT_SENSITIVE,
      sodium.crypto_pwhash_MEMLIMIT_SENSITIVE
    );
    const stringHash = bufferPasswordHash.toString('hex');
    sodium.sodium_munlock(bufferPasswordHash);
    return stringHash;
  }

  /**
   * Create a public/private key pair usable to create signatures
   *
   * @return {User}
   * @private
   */
  private createKeyPairSign(): User {
    /** @type {Buffer} */
    const bufferPrivateKey = sodium.sodium_malloc(sodium.crypto_sign_SECRETKEYBYTES);
    sodium.sodium_mlock(bufferPrivateKey);

    /** @type {Buffer} */
    this.bufferPublicKey = sodium.sodium_malloc(sodium.crypto_sign_PUBLICKEYBYTES);

    /** @type {Buffer} */
    this.bufferPrivateKeyEncrypted = sodium.sodium_malloc(bufferPrivateKey.length + sodium.crypto_secretbox_MACBYTES);

    /** @type {Buffer} */
    this.bufferNonce = sodium.sodium_malloc(sodium.crypto_secretbox_NONCEBYTES);
    sodium.randombytes_buf(this.bufferNonce);

    /** @type {Buffer} */
    const bufferSecret = sodium.sodium_malloc(sodium.crypto_secretbox_KEYBYTES);
    sodium.sodium_mlock(bufferSecret);
    bufferSecret.fill(this.bufferPassword.toString());

    sodium.crypto_sign_keypair(this.bufferPublicKey, bufferPrivateKey);

    sodium.crypto_secretbox_easy(this.bufferPrivateKeyEncrypted, bufferPrivateKey, this.bufferNonce, bufferSecret);

    KeyStore.make().set(this.identAccount + ':keyPrivate', bufferPrivateKey);
    sodium.sodium_munlock(bufferPrivateKey);
    sodium.sodium_munlock(bufferSecret);

    return this;
  }
}
