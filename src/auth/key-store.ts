/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import sodium from 'sodium-native';

export class KeyStore {
  private static instance: KeyStore;
  private keystore: Map<string, Buffer>;

  public static make(): KeyStore {
    if (!KeyStore.instance) {
      KeyStore.instance = new KeyStore();
    }
    return KeyStore.instance;
  }

  private constructor() {
    this.keystore = new Map();
  }

  public set(key: string, value: Buffer) {
    this.delete(key);
    if (!(value instanceof Buffer)) {
      value = Buffer.from(value);
    }
    const bufferValue = sodium.sodium_malloc(value.length);
    sodium.sodium_mlock(bufferValue);
    bufferValue.fill(value);
    value.fill(0);
    this.keystore.set(key, bufferValue);
  }

  public get(key: string): Buffer {
    return this.keystore.get(key) as Buffer;
  }

  public delete(key: string) {
    if (this.keystore.has(key)) {
      sodium.sodium_munlock(this.keystore.get(key) as Buffer);
      this.keystore.delete(key);
    }
  }

  public free() {
    this.keystore.forEach((v, k) => {
      sodium.sodium_munlock(v);
      this.keystore.delete(k);
    });
  }
}
