/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import { Db } from '../db';
import path from 'path';
import net from 'net';

export type Configuration = {
  ip_http_server?: string;
  port_http_server?: number;
  ip_protocol_server?: string;
  port_protocol_server?: number;
};

const DEFAULT_IP_HTTP_SERVER = '127.0.0.1';
const DEFAULT_PORT_HTTP_SERVER = 1972;

const DEFAULT_IP_PROTOCOL_SERVER = '127.0.0.1';
const DEFAULT_PORT_PROTOCOL_SERVER = 19720;

export class Config {
  private static instance: Config;

  public readonly path_app: string;
  public readonly VERSION: string;

  public readonly ip_http_server: string;
  public readonly port_http_server: number;

  public readonly ip_protocol_server: string;
  public readonly port_protocol_server: number;

  private db: Db;
  private data: { [key: string]: string | number } = {};

  /**
   * Factory, Singleton
   */
  public static make(config: Configuration = {} as Configuration): Config {
    Config.instance = Config.instance || new Config(config);
    return Config.instance;
  }

  private constructor(c: Configuration) {
    this.path_app = path.join(
      Object.keys(process).includes('pkg') ? path.dirname(process.execPath) : __dirname,
      '/../../'
    );
    this.VERSION = require(path.join(this.path_app, 'package.json')).version;

    this.ip_http_server = c.ip_http_server || process.env.IP_HTTP_SERVER || DEFAULT_IP_HTTP_SERVER;
    this.ip_http_server = net.isIP(this.ip_http_server) ? this.ip_http_server : DEFAULT_IP_HTTP_SERVER;
    this.port_http_server = Config.port(c.port_http_server || process.env.PORT_HTTP_SERVER || DEFAULT_PORT_HTTP_SERVER);

    this.ip_protocol_server = c.ip_protocol_server || process.env.IP_PROTOCOL_SERVER || DEFAULT_IP_PROTOCOL_SERVER;
    this.ip_protocol_server = net.isIP(this.ip_protocol_server) ? this.ip_protocol_server : DEFAULT_IP_PROTOCOL_SERVER;
    this.port_protocol_server = Config.port(
      c.port_protocol_server || process.env.PORT_PROTOCOL_SERVER || DEFAULT_PORT_PROTOCOL_SERVER
    );

    this.db = Db.connect();
    this.loadCache();
  }

  public getValueByKey(key: string): string | number | undefined {
    if (process.env.NODE_ENV === 'development') {
      const kd = key + '.' + process.env.NODE_ENV;
      key = typeof this.data[kd] !== 'undefined' ? kd : key;
    }
    if (typeof this.data[key] === 'undefined') {
      return undefined;
    }
    try {
      return JSON.parse(this.data[key].toString());
    } catch (error) {
      return this.data[key];
    }
  }

  public set(key: string, value: string | number) {
    this.db.insert(
      `REPLACE INTO config (key, value)
         VALUES (@key, @value)`,
      {
        key: key,
        value: value,
      }
    );
    this.loadCache();
  }

  private loadCache() {
    this.data = {};
    this.db.allAsArray('SELECT * FROM config').forEach((row) => {
      this.data[row.key] = row.value;
    });
  }

  /**
   * Number transformation
   * Boundaries
   */
  private static b(n: any, min: number, max: number): number {
    n = Number(n);
    min = Math.floor(min);
    max = Math.ceil(max);
    return n >= min && n <= max ? Math.floor(n) : n > max ? max : min;
  }

  private static port(n: any): number {
    return Number(n) ? Config.b(Number(n), 1025, 65535) : 0;
  }
}
