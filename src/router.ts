/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import { Express, Request, Response } from 'express';
import { Culture } from './view/culture';
import { Db } from './db';

export class Router {
  private package: any = require('../package.json');
  private readonly app: Express;
  private readonly db: Db;

  static make(app: Express) {
    return new Router(app);
  }

  private constructor(app: Express) {
    this.app = app;
    this.db = Db.connect();
    this.route();
  }

  private route() {
    this.app.get('/(|trade)$', (req: Request, res: Response) => {
      res.render('ux/trade', {
        contract: this.app.locals.Session.viewState.trade.contract || '',
        type: this.app.locals.Session.viewState.trade.type || '',
        arrayContract: [
          ...Culture.translateArray(
            this.db.allAsArray('SELECT contract_ident FROM contract').map((v) => v.contract_ident)
          ),
        ],
      });
    });

    this.app.get('/social', (req: Request, res: Response) => {
      res.render('ux/social', {
        arrayChatFriends: [],
        arrayMessage: [],
      });
    });

    this.app.get('/network', (req: Request, res: Response) => {
      res.render('ux/network', {});
    });

    this.app.get('/about', (req: Request, res: Response) => {
      res.render('ux/about', {
        version: this.package.version,
        license: this.package.license,
      });
    });

    this.app.get('/config', (req: Request, res: Response) => {
      res.render('ux/config', {});
    });

    this.app.post('/translate', (req: Request, res: Response) => {
      const objData: { [key: string]: Array<string[]> } = {};
      for (const type in req.body) {
        if (Object.prototype.hasOwnProperty.call(req.body, type) && Array.isArray(req.body[type])) {
          objData[type.toString()] = [...Culture.translateArray(req.body[type])];
        }
      }
      res.json(objData);
    });

    this.app.put('/trade/contract', (req: Request, res: Response) => {
      if (req.body.contract) {
        this.app.locals.Session.viewState.trade.contract = req.body.contract;
      }
      res.end();
    });

    this.app.put('/trade/type', (req: Request, res: Response) => {
      if (req.body.type) {
        this.app.locals.Session.viewState.trade.type = req.body.type;
      }
      res.end();
    });
  }
}
