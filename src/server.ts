/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import http, { Server as HttpServer } from 'http';
import { Logger } from './logger';
import express, { Express, NextFunction, Request, Response } from 'express';
import compression from 'compression';
import { Router } from './router';
import createError from 'http-errors';
import path from 'path';
import session from 'express-session';
import { Culture, CULTURE_DEFAULT_LANGUAGE_IDENT } from './view/culture';
import { SessionStore } from './view/session-store';
import { Config, Configuration } from './config/config';

export class Server {
  private config: Config;

  private readonly app: Express;
  private readonly httpServer: HttpServer;

  constructor(c?: Configuration) {
    this.config = Config.make(c);
    Logger.info(`diva ${this.config.VERSION} instantiating...`);

    // express application
    this.app = express();

    // hide express
    this.app.set('x-powered-by', false);

    // catch unavailable favicon.ico
    this.app.get('/favicon.ico', (req, res) => res.sendStatus(204));

    // compression
    this.app.use(compression());

    // static content
    this.app.use(express.static(path.join(__dirname, '/../view/static/')));

    // support JSON
    this.app.use(express.json());

    // session
    this.app.use(
      session({
        cookie: {
          httpOnly: true,
          secure: false,
          sameSite: 'strict',
          maxAge: 60 * 1000, //@FIXME this expiry is too short
        },
        name: 'diva',
        resave: false,
        saveUninitialized: false,
        secret: SessionStore.getSessionKey('diva'),
        store: SessionStore.make(),
      })
    );

    this.app.use((req: Request, res: Response, next: NextFunction) => {
      const uiLanguage: string =
        req.body && req.body.uiLanguage
          ? req.body.uiLanguage.toString()
          : req.query && req.query.uiLanguage
          ? req.query.uiLanguage.toString()
          : req.session && req.session.viewState && req.session.viewState.uiLanguage
          ? req.session.viewState.uiLanguage
          : req.acceptsLanguages(Culture.getListIdentLanguages()) || CULTURE_DEFAULT_LANGUAGE_IDENT;
      this.app.locals.Session = req.session || null;
      this.app.locals.Session.viewState = this.app.locals.Session.viewState || {};
      this.app.locals.Session.viewState.trade = this.app.locals.Session.viewState.trade || {};
      this.app.locals.Session.viewState.uiLanguage = uiLanguage;
      this.app.locals.Culture = Culture.make(uiLanguage);
      next();
    });

    // Pug
    this.app.set('views', path.join(__dirname, '/../view/'));
    this.app.set('view engine', 'pug');

    // init API
    Router.make(this.app);
    Logger.info('Router initialized');

    // catch 404 and forward to error handler
    this.app.use((req: Request, res: Response, next: NextFunction) => {
      next(createError(404));
    });

    // error handler
    this.app.use(Server.error);

    // Web Server
    this.httpServer = http.createServer(this.app);
    this.httpServer.on('listening', () => {
      Logger.info(`HttpServer listening on ${this.config.ip_http_server}:${this.config.port_http_server}`);
    });
    this.httpServer.on('close', () => {
      Logger.info(`HttpServer closing on ${this.config.ip_http_server}:${this.config.port_http_server}`);
    });
  }

  public async start() {
    await this.httpServer.listen(this.config.port_http_server, this.config.ip_http_server);
  }

  public async shutdown() {
    return new Promise((resolve) => {
      this.httpServer.close(resolve);
    });
  }

  private static error(err: any, req: Request, res: Response, next: NextFunction) {
    res.status(err.status || 500);

    if (req.accepts('html')) {
      res.render('error', {
        title: 'ERROR',
        status: err.status || 500,
        message: err.message || 'Error',
        error: err,
      });
    }

    next();
  }
}
