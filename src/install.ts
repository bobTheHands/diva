/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import { Config } from './config/config';
import { Db } from './db';
import fs from 'fs-extra';
import path from 'path';
import { Logger } from './logger';

(async () => {
  try {
    fs.unlinkSync(path.normalize(path.join(__dirname, '../data/diva.sqlite')));
  } catch (error: any) {
    Logger.trace(error.toString());
  }
  try {
    fs.unlinkSync(path.normalize(path.join(__dirname, '../view/static/vendor/umbrella.min.js')));
  } catch (error: any) {
    Logger.trace(error.toString());
  }

  fs.copyFileSync(
    path.normalize(path.join(__dirname, '../node_modules/umbrellajs/umbrella.min.js')),
    path.normalize(path.join(__dirname, '../view/static/vendor/umbrella.min.js'))
  );

  Db.create('diva');
  const config = Config.make();
  process.env.API && config.set('api', process.env.API);
  process.env.API_TOKEN && config.set('api.token', process.env.API_TOKEN);
  process.env.I2P_HTTP_PROXY && config.set('i2p.http.proxy', process.env.I2P_HTTP_PROXY);
  process.env.I2P_SOCKS_PROXY && config.set('i2p.socks.proxy', process.env.I2P_SOCKS_PROXY);
})();
