/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import crypto from 'crypto';
import fs from 'fs-extra';
import path from 'path';
import { Store, CookieOptions, SessionData } from 'express-session';

// [declaration merging](https://www.typescriptlang.org/docs/handbook/declaration-merging.html)
declare module 'express-session' {
  export interface SessionData {
    viewState: {
      uiLanguage: string;
      trade: {
        contract: string;
        type: string;
      }
    }
  }
}

const SESSION_STORE_PATH_DEFAULT = path.join(__dirname, '../../data/session/');
const ERROR_SESSION_STORE_PATH_INVALID = 'invalid path';

export class SessionStore extends Store {
  private readonly path: string;

  public static getSessionKey(nameSession: string, pathKey: string = '') {
    const pathSessionKey = path.join(pathKey || SESSION_STORE_PATH_DEFAULT, '/', nameSession + '.session.key');
    let sessionKey;
    try {
      sessionKey = fs.readFileSync(pathSessionKey).toString();
    } catch (error) {
      // buffer length between 16 and 32 bytes
      const _buf = Buffer.alloc(Math.floor(Math.random() * 16 + 16));
      sessionKey = crypto.randomFillSync(_buf).toString();
      fs.outputFileSync(pathSessionKey, sessionKey, { mode: 0o600 });
    }

    return sessionKey;
  }

  public static make(options: CookieOptions = {}): SessionStore {
    return new SessionStore(options);
  }

  private constructor(options: CookieOptions) {
    super();

    options.path = options && options.path ? options.path : SESSION_STORE_PATH_DEFAULT;
    try {
      fs.ensureDirSync(options.path, { mode: 0o700 });
    } catch (error) {
      throw new Error(ERROR_SESSION_STORE_PATH_INVALID);
    }
    this.path = options.path;
  }

  public set(sid: string, session: SessionData, callback?: Function) {
    let error = null;
    const p = this.getSessionPath(sid);
    try {
      fs.removeSync(p);
      fs.outputFileSync(p, session ? JSON.stringify(session) : '', {
        mode: 0o600,
        flag: 'wx',
      });
    } catch (err) {
      error = err;
    }
    callback && callback(error);
  }

  public get(sid: string, callback?: Function) {
    fs.readFile(this.getSessionPath(sid), { flag: 'r' }, (err, data) => {
      callback && callback(err, data ? JSON.parse(data.toString()) : null);
    });
  }

  public destroy(sid: string, callback?: Function) {
    let error = null;
    try {
      fs.removeSync(this.getSessionPath(sid));
    } catch (err) {
      error = err;
    }
    callback && callback(error);
  }

  public length(callback?: Function) {
    fs.readdir(this.path, (err, files) => {
      callback && callback(err, files.filter((path) => path.indexOf('.') === -1).length);
    });
  }

  public clear(callback?: Function) {
    let error = null;
    try {
      fs.readdirSync(this.path)
        .filter((path) => path.indexOf('.') === -1)
        .forEach((p) => fs.unlinkSync(path.join(this.path, p)));
    } catch (err) {
      error = err;
    }
    callback && callback(error);
  }

  public touch(sid: string, session: SessionData, callback?: Function) {
    this.set(sid, session, callback);
  }

  private getSessionPath(sid: string) {
    return path.join(this.path, crypto.createHash('sha256').update(sid).digest('hex'));
  }
}
