/**
 * Copyright (C) 2021 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

import dateFormat from 'dateformat';
import { Db } from '../db';

export const CULTURE_DEFAULT_LANGUAGE_IDENT = 'en';

export class Culture {
  public static uiLanguage: string;
  private static db: Db;
  private static cacheTranslation: Map<string, string>;
  private static cacheIdentLanguages: Array<string>;

  public static make(uiLanguage?: string) {
    Culture.init();
    Culture.uiLanguage = uiLanguage || CULTURE_DEFAULT_LANGUAGE_IDENT;
    return Culture;
  }

  public static getListIdentLanguages() {
    Culture.init();
    return Culture.cacheIdentLanguages;
  }

  public static reload(): Culture {
    return Culture.init(true);
  }

  public static translateString(ident: string, identLanguage: Array<string> | string = ''): string {
    return Culture.translate([ident], identLanguage).values().next().value;
  }

  /**
   * Alias for translateString or translatArray
   */
  public static t(
    ident: Array<string> | string,
    identLanguage: Array<string> | string = ''
  ): string | Map<string, string> {
    return typeof ident === 'string'
      ? Culture.translateString(ident, identLanguage)
      : Culture.translateArray(ident, identLanguage);
  }

  public static translateArray(ident: Array<string>, identLanguage: Array<string> | string = ''): Map<string, string> {
    return Culture.translate(ident, identLanguage);
  }

  public static formatDateTime(timestamp: number, uiLanguage: string = Culture.uiLanguage, format: string = '') {
    if (format !== '') {
      return dateFormat(timestamp, format);
    }
    switch (uiLanguage) {
      case 'de':
        return dateFormat(timestamp, 'dd.mm.yyyy HH:MM:ss.l o');
      case 'en':
      default:
        return dateFormat(timestamp, 'mm/dd/yyyy hh:MM:ss.l TT o');
    }
  }

  public static translate(ident: Array<string>, identLanguage: Array<string> | string): Map<string, string> {
    Culture.init();

    // unify
    ident = [...new Set(ident)];

    let arrayIdentLanguage = typeof identLanguage === 'string' ? [identLanguage] : identLanguage;
    arrayIdentLanguage.push(Culture.uiLanguage, CULTURE_DEFAULT_LANGUAGE_IDENT);
    // remove duplicates and empty values
    arrayIdentLanguage = [...new Set(arrayIdentLanguage)].filter((v) => v);

    const mapTranslation = new Map();
    ident.forEach((i) => {
      let translation = '';
      if (i.match(/^[0-9]+$/)) {
        translation = Culture.formatDateTime(parseInt(i), arrayIdentLanguage[0]);
      } else {
        const _iL = arrayIdentLanguage.slice(0);
        while (_iL.length > 0 && !translation) {
          translation = Culture.cacheTranslation.get(_iL.shift() + ':' + i) || '';
        }
      }
      mapTranslation.set(String(i), translation || i);
    });

    return mapTranslation;
  }

  private static init(force: boolean = false): Culture {
    if (typeof Culture.db === 'undefined' || force) {
      Culture.db = Db.connect();

      Culture.cacheTranslation = new Map(
        Culture.db.allAsArray('SELECT * FROM culture').map((r) => [r.language_ident + ':' + r.ident, r.text])
      );
      Culture.cacheIdentLanguages = Culture.db
        .allAsArray('SELECT language_ident FROM language')
        .map((r) => r.language_ident);
    }

    return Culture;
  }
}
